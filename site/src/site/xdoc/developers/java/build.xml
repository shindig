<?xml version="1.0" encoding="UTF-8"?>

<!--
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
-->

<document xmlns="http://maven.apache.org/XDOC/2.0"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/XDOC/2.0 http://maven.apache.org/xsd/xdoc-2.0.xsd">
  <properties>
    <title>Building Java Shindig</title>
    <author>Vincent Siveton</author>
    <date>2009-03-25</date>
  </properties>
  <body>
    <section name="Building and running Shindig for Java">
      <p>This is the Java steps on how to build and run Shindig.</p>

      <ul>
        <li>
          <a href="#Prequisites_before_building_Shindig">Prequisites</a>
        </li>
        <li>
          <a href="#Getting_the_code">Get the code</a>
        </li>
        <li>
          <a href="#Building_and_running_the_code_with_Maven">Build and run the code (with
            Maven)</a>
        </li>
        <li>
          <a href="#Setting_up_an_Eclipse_project_to_build_Shindig">Setting up an Eclipse
            project</a>
        </li>
        <li>
          <a href="#Generating_Code_Coverage_in_Eclipse">Generating Code Coverage in
            Eclipse</a>
        </li>
        <li>
          <a href="#Running_inside_Eclipse">Running inside Eclipse</a>
        </li>
        <li>
          <a href="#Running_with_Caja">Running with Caja</a>
        </li>
      </ul>

      <subsection name="Prequisites before building Shindig">
        <p>In order to build Shindig, you must have the following:</p>
        <ul>
          <li>Java (JDK/JRE) 1.5 or later installed on your system and the JAVA_HOME
            environment variable set. <ul>
              <li>See: <a href="http://java.sun.com/">http://java.sun.com/</a> for
                installation instructions.</li>
            </ul>
          </li>
          <li>A Subversion client installed in order to checkout the code. <ul>
              <li>Instructions for downloading and installing Subversion can be found
                here: <a href="http://subversion.tigris.org/"
                  >http://subversion.tigris.org/</a></li>
            </ul>
          </li>
          <li>Apache Maven installed to perform the build. <ul>
              <li>Instructions for downloading and installing Maven can be found here:
                  <a href="http://maven.apache.org/download.html"
                  >http://maven.apache.org/download.html</a></li>
            </ul>
          </li>
        </ul>
      </subsection>

      <subsection name="Getting the code">
        <p>Create a subdirectory and checkout the Shindig code from its Subversion
          repository</p>
        <ol>
          <li><code>mkdir ~/src/shindig</code> (or wherever you'd like to put it)</li>
          <li>
            <code>cd ~/src/shindig</code>
          </li>
          <li>
            <code>svn co http://svn.apache.org/repos/asf/incubator/shindig/trunk/
            .</code>
          </li>
        </ol>
      </subsection>

      <subsection name="Building and running the code with Maven">
        <p>To build a Web Archive (WAR) file for the Gadget server and run tests, perform
          the following:</p>
        <ol>
          <li>Make sure you have the <a href="#preReqs">prerequisites</a> installed first.</li>
          <li>
            <code>cd ~/src/shindig/</code>
          </li>
          <li>
            <code>mvn</code>
          </li>
          <li>Once the build successfully completes, you can install the built WAR files
            located in the /target subdirectory onto your JEE server.</li>
        </ol>
        <p>To run the code and start a Jetty server that will run on at localhost:8080:</p>
        <ul>
          <li>
            <code>mvn -Prun</code>
          </li>
          <li> Open in you browser <a
              href="http://localhost:8080/gadgets/ifr?url=http://www.labpixies.com/campaigns/todo/todo.xml"
              >http://localhost:8080/gadgets/ifr?url=http://www.labpixies.com/campaigns/todo/todo.xml</a>
          </li>
        </ul>
        <p>To run the Jetty server on a different port, use:</p>
        <ul>
          <li>
            <code>cd java/server</code>
          </li>
          <li>
            <code>mvn clean install jetty:run
              -DrunType=&lt;full|gadgets|social&gt;
              -Djetty.port=&lt;port&gt;</code>
          </li>
        </ul>

        <p>Once you've either installed the WAR file on your JEE server, or are running
          locally using the Jetty server, you can test the Gadget server using:</p>
        <ul>
          <li>http://localhost:&lt;port&gt;/gadgets/ifr?url=http://www.labpixies.com/campaigns/todo/todo.xml</li>
        </ul>

        <p><img src="../../images/samples/task.png" alt="Sample Container"/></p>
      </subsection>

      <subsection name="Setting up an Eclipse project to build Shindig">
        <p>These steps, after completing the previous section, will allow you to build from
          within Eclipse using the Maven2 plugin. You should first install the Maven
          plugin, then create the new Java project.</p>
        <ul>
          <li>Create <code>~/.m2/settings.xml</code> consisting solely of</li>
        </ul>
        <source> &lt;settings&gt; &lt;/settings&gt;</source>
        <ul>
          <li>Install the Maven2 plugin <ol>
              <li>Help -&gt; Software Updates -&gt; Find and Install</li>
              <li>Search for new features to install</li>
              <li>Create a new remote update site for the Maven 2 plugin <ul>
                  <li>Name: Maven2 - Sonatype</li>
                  <li>URL: <code>http://m2eclipse.sonatype.org/update/</code></li>
                </ul>
              </li>
              <li>Select the site and click "Finish"</li>
              <li>There are optional dependencies on mylyn and subclipse. If you don't
                have these plugins, you can get them <a
                  href="http://m2eclipse.sonatype.org/update/"
                  title="Maven Integration for Eclipse updates">here</a>.
                Otherwise, select only the Maven Integration plug-in.</li>
              <li>Complete the installation</li>
            </ol>
          </li>
        </ul>
        <ul>
          <li>Setup new workspace and project<br/> Creating a new workspace eliminates the
            performance cost from existing projects and makes it easier to manage the
            code. <ol>
              <li>File -&gt; Switch Workspace -&gt; Other...</li>
              <li>Select directory to store workspace <ul>
                  <li>Do not select a parent directory of the shindig source (e.g.
                    ~/src/shindig) as Eclipse won't allow you to create the Java
                    project.</li>
                  <li>Something like <code>~/eclipse/workspaces/shindig</code>
                    would work fine</li>
                </ul>
              </li>
              <li>File -&gt; New -&gt; Java Project <ol>
                  <li>Name the project. The instructions below will assume
                    "SHINDIG".</li>
                  <li>Select 'Create project from existing source' and navigate to
                      <code>.../src/shindig/java</code>
                  </li>
                  <li>Click Finish</li>
                  <li>If you see a dialog for "Open Associated Perspective", click
                    Ok. Don't worry about the errors after loading as they will
                    be fixed in the next step.</li>
                </ol>
              </li>
              <li>Right-click the project, select <code>Maven : Enable Dependency
                  Management</code></li>
              <li>Right-click the project, select <code>Maven : Update Source
                Folders</code></li>
              <li>Optionally, if you would like to be able to browse or step into the
                code of your dependent jars when debugging, you need the source
                jars. Right-click the project, select <code>Maven : Download
                Sources</code> and Eclipse will automatically know about these
                sources when debugging. You can browse them under <code>Maven
                  Dependencies</code> in your project.</li>
              <li>If you'll be using AllTests to run tests or generate code coverage
                stats, adjust the project's output folders. <ol>
                  <li>Project -&gt; Properties -&gt; Java Build Path
                    -&gt; Source</li>
                  <li>Locate and open <code>SHINDIG/gadgets/src/test/java</code></li>
                  <li>Select <code>Output Folder: (Default Output Folder)</code>
                    and click Edit...</li>
                  <li>Select <code>Specific Output Folder</code></li>
                  <li>Enter <code>target/test-classes</code> and click OK.</li>
                  <li>Repeat for
                  <code>SHINDIG/social-api/src/test/java</code></li>
                </ol>
              </li>
            </ol>
          </li>
        </ul>
      </subsection>

      <subsection name="Generating Code Coverage in Eclipse ">
        <p>To generate code coverage statistics inside of Eclipse, install the <a
            href="http://www.eclemma.org">EclEmma</a> plugin. Then</p>
        <ul>
          <li>Open <code>org.apache.shindig.gadgets.AllTests</code></li>
          <li>Right-click in the class, and select <code>Coverage as -&gt; JUnit
            Test</code></li>
        </ul>
      </subsection>

      <subsection name="Running inside Eclipse">
        <p>To debug the server in Eclipse, follow the last two steps <a
            href="http://cwiki.apache.org/WICKET/maven-jetty-plugin.html" target="_top"
            >here</a> (takes a few minutes to set up):</p>
        <ul>

          <li>"Using eclipse external tools"</li>
          <li>"Attaching to the server running in debug mode, using eclipse"</li>
        </ul>
        <p><b>Note:</b> You must have set up Eclipse to build the code or do <code>mvn
            package</code> yourself after making changes, but you won't need to restart
          Jetty to see your changes.</p>
      </subsection>

      <subsection name="Running with Caja">
        <p>Caja is an important part of OpenSocial that greatly enhances JavaScript
          security. Caja is managed in a separate open source project hosted by Google
          code projects. For more information on Caja, see: <a
            href="http://code.google.com/p/google-caja/wiki/CajaEasyIntro"
            >http://code.google.com/p/google-caja/wiki/CajaEasyIntro</a>
        </p>
        <ol>
          <li>Load this page: <a
              href="http://localhost:8080/gadgets/files/samplecontainer/samplecontainer.html"
              target="_top"
              >http://localhost:8080/gadgets/files/samplecontainer/samplecontainer.html</a></li>

          <li>Point it to this gadget: <a
              href="http://localhost:8080/gadgets/files/samplecontainer/examples/SocialHelloWorld.xml"
              target="_top"
              >http://localhost:8080/gadgets/files/samplecontainer/examples/SocialHelloWorld.xml</a>
          </li>
        </ol>

        <p>To see the cajoled code (Firefox only), right-click inside the iframe and do
          "This Frame -&gt; View Frame Source"</p>
      </subsection>

      <subsection name="Additional reading">
        <p> Read <a
            href="http://svn.apache.org/repos/asf/incubator/shindig/trunk/java/README"
            >java/README</a> for original instructions on how to start up any of the
          java shindig servers.</p>
        <p>Read <a
            href="http://svn.apache.org/repos/asf/incubator/shindig/trunk/javascript/README"
            >javascript/README</a> for instructions for using the Shindig Gadget
          Container JavaScript to enable your page to render Gadgets using gmodules.com or
          a server started up as described above.</p>
      </subsection>
    </section>
  </body>
</document>
