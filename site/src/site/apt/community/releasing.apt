 -----
 Releasing Shindig
 -----
 Vincent Siveton
 ------
 2009-04-18
 ------

~~ Licensed to the Apache Software Foundation (ASF) under one
~~ or more contributor license agreements.  See the NOTICE file
~~ distributed with this work for additional information
~~ regarding copyright ownership.  The ASF licenses this file
~~ to you under the Apache License, Version 2.0 (the
~~ "License"); you may not use this file except in compliance
~~ with the License.  You may obtain a copy of the License at
~~
~~   http://www.apache.org/licenses/LICENSE-2.0
~~
~~ Unless required by applicable law or agreed to in writing,
~~ software distributed under the License is distributed on an
~~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
~~ KIND, either express or implied.  See the License for the
~~ specific language governing permissions and limitations
~~ under the License.

~~ NOTE: For help with the syntax of this file, see:
~~ http://maven.apache.org/doxia/references/apt-format.html

Shindig Release Process

  This document describes how Shindig's committers will make a release.

%{toc}

  <<Notes>>:

    [[1]] This process is based on the {{{http://maven.apache.org/developers/release/releasing.html}Maven Release Process}}.
      Please read it firstly for the prerequisites and technical notes.

    [[2]] The items 2 to 5 should take you 1-2 hours, depending your machine and your Internet connection.

    []

* {1. Release Discussions}

  The starting point will be a consensus in the developer community about the release and the JIRA issues to include
  (or not) in the next release. Typically, the discussion will be on the {{{../mail-lists.html}Dev List}}.

* {2. Prepare The Release}

  <<Duration>>: 20 min

  Now that everybody in the community is happy to push a release, the release manager needs to verify the source code
  before to continue:

  [[1]] Make sure there are no snapshots (less the current version) in the POMs to be released.

  [[2]] Check that your POMs will not lose content when they are rewritten during the release process.

    [[A]] <<<mvn release:prepare -DdryRun=true -Prelease>>>

    [[B]] Diff the original file pom.xml with the one called pom.xml.tag to see if the license or any other info has
      been removed. The only things that should be different between these files are the <version> and <scm> elements.
      Any other changes, you must backport yourself to the original pom.xml file and commit before proceeding with the
      release.

    [[C]] Verify that all Shindig modules will be touched, including <<<assembly>>> and <<<samples>>>.

    []

  []

  Next, prepare the release by calling Maven:

+-----+
mvn release:clean -Prelease
mvn release:prepare -Prelease
+-----+

  <<Notes>>:

    [[1]] <-Prelease> is <<very>> important since <<<assembly>>> and <<<samples>>> modules are not included by
      default in the build (for productivity reasons).

    [[2]] Preparing the release will create a new tag in SVN, automatically checking in on your behalf. Verify the
      SVN logs in the {{{../mail-lists.html}Commit List}} or review the generated tag.

    [[3]] All license headers and legal files will be checked by RAT (Release Audit Tool).

    []

* {3. Stage The Release}

  <<Duration>>: 30 min

  Call Maven to publish the generated artifacts:

+-----+
mvn release:perform -Prelease
+-----+

  <<Note>>: All artifacts will be signed and pushed to {{https://repository.apache.org/}}.

  Close the staging repository as described in the points 5 and 6 of the
  {{{http://maven.apache.org/developers/release/releasing.html}Maven Release Process}}.

* {4. Deploy The Documentation}

  <<Duration>>: 20 min

  Call Maven to publish the generated technical site:

+-----+
cd target/checkout
mvn site -Preporting
mvn site:deploy
+-----+

  <<Optional>>: Redeploy the current website:

+-----+
cd trunk/site
mvn clean site
mvn site:deploy
+-----+

  <<Note>>: Wait for the sync before to see the changes in {{http://incubator.apache.org/shindig/}}.

* {5. Propose A Vote}

  Start a release vote on the {{{../mail-lists.html}Dev List}}. The vote must be aligned with the
  {{{http://www.apache.org/foundation/voting.html}Apache}} vote process.

  Typically, the mail should include the
  {{{http://www.atlassian.com/software/jira/docs/latest/releasenotes.html}Jira release notes}}, the staging
  repository URL and the site URL, for instance:

+-----+
To: "Shindig Developers List" <shindig-dev@incubator.apache.org>
Subject: [VOTE] Release Apache Incubator Shindig version X.Y

Hi,

We solved N issues:
https://issues.apache.org/jira/secure/ReleaseNote.jspa?version=XXX&styleName=Html&projectId=12310741

There are still a couple of issues left in JIRA:
https://issues.apache.org/jira/secure/IssueNavigator.jspa?reset=true&pid=12310741&status=1

Staging repo:
https://repository.apache.org/content/repositories/shindig-staging-[YOUR REPOSITORY ID]/

Web site:
http://incubator.apache.org/shindig/

Vote open for 72 hours.

[ ] +1
[ ] +0
[ ] -1

+-----+

* {6. Ask Incubator PMC}

  After a successful release vote on the Dev list, the release needs to be approved by the Incubator PMC. This is
  necessary since Shindig is still in the Apache Incubator. For details about this approval look at the
  {{{http://incubator.apache.org/incubation/Incubation_Policy.html#Podling+Constraints}Incubation Podling Constraints}}.
  The vote for the Incubator PMC must be started on the
  {{{http://incubator.apache.org/guides/lists.html}General Incubator list}}. The vote mail must contain SVN and deploy
  links to the release artifacts as they will be published and a link to the vote thread on the Dev list.

* {7. Publish The Release}

  Promote the release as described in the points 10 of the
  {{{http://maven.apache.org/developers/release/releasing.html}Maven Release Process}}.

  Also, update Jira to specify the release date.

* {8. Publish The Website}

  Update the {{{https://svn.apache.org/repos/asf/incubator/shindig/trunk/site/src/site/apt/download/index.apt}download page}}
  and redeploy the website (need to sync):

+-----+
cd trunk/site
mvn clean site
mvn site:deploy
+-----+

* {9. Announce The Release}

  Create an announcement similar to:

+-----+
From: YOUR_APACHE_USERNAME@apache.org
To: announce@apache.org, shindig-dev@incubator.apache.org
Subject: [ANN] Apache Incubator Shindig X.Y Released

The Apache Incubator Shindig team is proud to announce the release
of Apache Incubator Shindig, version X.Y-incubating.

Apache Incubator Shindig is a JavaScript container and
implementations of the backend APIs and proxy required for hosting
OpenSocial applications.

http://incubator.apache.org/shindig

Apache Shindig is an effort undergoing incubation at The Apache
Software Foundation (ASF), sponsored by the Apache Incubator
project. Incubation is required of all newly accepted projects
until a further review indicates that the infrastructure,
communications, and decision making process have stabilized in a
manner consistent with other successful ASF projects. While
incubation status is not necessarily a reflection of the
completeness or stability of the code, it does indicate that the
project has yet to be fully endorsed by the ASF.

Enjoy,

-The Apache Incubator Shindig Team

+-----+

* {10. Celebrate :o)}
